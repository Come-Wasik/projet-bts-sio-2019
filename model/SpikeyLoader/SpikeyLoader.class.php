<?php
require_once(__DIR__.'/SpikeyPage.class.php');

class SpikeyLoader {
    private $getActionName = 'act';
    private $mainPageName = 'main';
    private $pages = [];

    private const NB_MAIN_PAGE = 1;

    public function __construct(string $inputMainPageName = '', $loadMode = SpikeyPage::CONTROL_AND_SHOW) {
        if(empty($inputMainPageName))
            $inputMainPageName = $this->mainPageName;
        else
            $this->mainPageName = $inputMainPageName;

        if($this->addPage(new SpikeyPage($this->mainPageName, $loadMode, null)) == false)
            header('location: index.php');
    }

    public function addPage(SpikeyPage $inputPage) : bool {
        foreach($this->pages as $page) {
            if($page->getName() == $inputPage->getName())
                return false;
        }
        $this->pages[] = $inputPage;
        return true;
    }

    public function start() : bool {
        if($this->noSpecificPage($_GET[$this->getActionName] ?? '')) {
            // Afficher la page principale
            $this->selectLoader($this->getMainPage());
            
        }
        else if($this->pageSpecificExist($_GET[$this->getActionName])) {
            //Test des conditions
            foreach($this->pages as $page) {
                if($_GET[$this->getActionName] == $page->getName()) {
                    $usedPage = $page;
                    $executeCondition = $usedPage->getCondition();
                    if($executeCondition != null)
                        $executeCondition();
                }
            }
            $this->selectLoader($usedPage);
        }
        else {
            //Page non trouvé
            echo 'Page non trouvé';
        }
        return true;
    }

    private function selectLoader($usedPage) : void {
        if($usedPage->getLoadMode() == SpikeyPage::STATIC_PAGE) {
            require __DIR__.'/../../view/'.$usedPage->getName().'.view.php';
        }
        else { // $usedPage == SpikeyPage::CONTROL_ONLY
            ('page'.ucfirst($usedPage->getName()))();
        }
    }
    
    private function getMainPage() : SpikeyPage {
        foreach($this->pages as $page) {
            if($page->getName() == $this->mainPageName)
                return $page;
        }
    }

    private function noSpecificPage(string $specificPage) : bool {
        if(empty($specificPage))
            return true;
        else
            return false;
    }

    private function pageSpecificExist($specificPage) : bool {
        if((count($this->pages) - SpikeyLoader::NB_MAIN_PAGE) == 0)
            return false;

        foreach($this->pages as $page) {
            if($specificPage == $page->getName())
                return true;
        }
        return false;
    }

    static public function getViewDIR() : string {
        return __DIR__.'/../../view/';
    }
}