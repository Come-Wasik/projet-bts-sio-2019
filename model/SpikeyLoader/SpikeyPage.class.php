<?php
class SpikeyPage {
    private $name;
    private $condition;
    private $loadMode;

    public const STATIC_PAGE = 0;
    public const DYNAMIC_PAGE = 1;

    public function __construct(string $pageName, int $loadMode, callable $condition = null) {
        $this->name = $pageName;
        if(!empty($condition))
            $this->condition = $condition;
        $this->loadMode = ($loadMode >= 0 && $loadMode <= 2 ? $loadMode : SpikeyPage::CONTROL_AND_SHOW);
    }

    public function getName() : string { return $this->name; }
    public function getCondition()  { return $this->condition; }
    public function getLoadMode() { return $this->loadMode; }
    public function setName(string $name) : void { $this->name = $name; }
    public function setCondition( $condition) : void { $this->condition = $condition; }
}