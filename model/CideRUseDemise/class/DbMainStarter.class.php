<?php

class DbMainStarter {
  /*--------------------------------------
  | Developped by Côme Wasik / nolikein |
  -------------------------------------*/
  /*-------------
  | Item Diagram |
  ---------------
    Description :
    -------------
        This class allow to get one Database on PDO in multiple formats

    Follow the package :
    --------------------
        You can follow the update here : https://github.com/Nolikein/CideRUseDemise

    Méthodes :
    ----------
        +s useMySqlDB() : PDO

    DEBUG NOTE :
    ------------

        WARNING 001 : This is just possible to open connexion to Mysql. No MySqli
    */

    const HOST = 'localhost';
    const DBNAME = 'form2Pdf';
    const USER = 'root';
    const PASSWORD = '';

    public static function useMySqlDb(  $host = DbMainStarter::HOST, $dbname = DbMainStarter::DBNAME,
                                        $user = DbMainStarter::USER, $password = DbMainStarter::PASSWORD) {
        return new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8', $user, $password);
    }
}