<?php
/*--------------------------------------
  | Developped by Côme Wasik / nolikein |
  -------------------------------------*/
  /*-------------
  | Item Diagram |
  ---------------
      Description :
      -------------
        This file allow to initialize the package. You must include it if you would use it.

      Follow the package :
      --------------------
        You can follow the update here : https://github.com/Nolikein/CideRUseDemise
*/

foreach(['CideRUseDemise', 'DbMainStarter', 'GetDataClass'] as $file) {
  require_once(__DIR__.'/class/'. $file .'.class.php');
}

interface iManageableObject {
  public function __construct(array $data);
  public function getPrimaryKey() : string;
  public function PKIsAutoIncremented() : bool;
  public function getTableName() : string;
}
