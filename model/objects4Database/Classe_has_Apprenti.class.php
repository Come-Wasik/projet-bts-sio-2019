<?php
class Classe_has_Apprenti implements iManageableObject {
    private $idTable;
    private $idClasse;
    private $idApprenti;
    private $anneeActuelle;

    public function __construct(array $data) {
      $this->hydrate($data);
    }

    private function hydrate(array $data) {
        foreach($data as $key => $value) {
        $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getIdTable() { return $this->idTable; }
    public function getIdClasse() { return $this->idClasse; }
    public function getIdApprenti() { return $this->idApprenti; }
    public function getAnneeActuelle() { return $this->anneeActuelle; }

    public function setIdTable(int $newData) { $this->idTable = $newData; }
    public function setIdClasse(int $newData) { $this->idClasse = $newData; }
    public function setIdApprenti(int $newData) { $this->idApprenti = $newData; }
    public function setAnneeActuelle(int $newData) { $this->anneeActuelle = $newData; }

    public function getPrimaryKey() : string { return 'idTable'; }
    public function PKIsAutoIncremented() : bool { return true; }
    public function getTableName() : string { return 'Classe_has_Apprenti'; }
}