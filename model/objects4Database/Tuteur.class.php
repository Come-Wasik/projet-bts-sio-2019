<?php

class Tuteur implements iManageableObject {
    private $idTuteur;
    private $nomTuteur;
    private $telTuteur;
    private $mailTuteur;
    private $idEntreprise;


    public function __construct(array $data) {
      $this->hydrate($data);
    }

    private function hydrate(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getIdTuteur() { return $this->idTuteur; }
    public function getNomTuteur() { return $this->nomTuteur; }
    public function getTelTuteur() { return $this->telTuteur; }
    public function getMailTuteur() { return $this->mailTuteur; }
    public function getIdEntreprise() { return $this->idEntreprise; }

    public function setIdTuteur(int $newData) { $this->idTuteur = $newData; }
    public function setNomTuteur(String $newData) { $this->nomTuteur = $newData; }
    public function setTelTuteur(String $newData) { $this->telTuteur = $newData; }
    public function setMailTuteur(String $newData) { $this->mailTuteur = $newData; }
    public function setIdEntreprise(int $newData) { $this->idEntreprise = $newData; }

    public function getPrimaryKey() : string { return 'idTuteur'; }
    public function PKIsAutoIncremented() : bool { return true; }
    public function getTableName() : string { return 'Tuteur'; }
}