<?php
class Entreprise implements iManageableObject {
    private $idEntreprise;
    private $nomEntreprise;

    public function __construct(array $data) {
      $this->hydrate($data);
    }

    private function hydrate(array $data) {
        foreach($data as $key => $value) {
        $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getIdEntreprise() { return $this->idEntreprise; }
    public function getNomEntreprise() { return $this->nomEntreprise; }
    
    public function setIdEntreprise(int $newData) { $this->idEntreprise = $newData; }
    public function setNomEntreprise(String $newData) { $this->nomEntreprise = $newData; }

    public function getPrimaryKey() : string { return 'idEntreprise'; }
    public function PKIsAutoIncremented() : bool { return true; }
    public function getTableName() : string { return 'Entreprises'; }
}