<?php
class Apprenti implements iManageableObject {
    private $idApprenti;
    private $prenomApprenti;
    private $nomApprenti;
    private $idEntreprise;
    private $idTuteur;
    private $nbRetards;
    private $nbAbsences;

    public function __construct(array $data) {
      $this->hydrate($data);
    }

    private function hydrate(array $data) {
        foreach($data as $key => $value) {
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getIdApprenti() { return $this->idApprenti; }
    public function getPrenomApprenti() { return $this->prenomApprenti; }
    public function getNomApprenti() { return $this->nomApprenti; }
    public function getIdEntreprise() { return $this->idEntreprise; }
    public function getIdTuteur() { return $this->idTuteur; }
    public function getNbRetards() { return $this->nbRetards; }
    public function getNbAbsences() { return $this->nbAbsences; }
    
    public function setIdApprenti(int $newData) { $this->idApprenti = $newData; }
    public function setPrenomApprenti(String $newData) { $this->prenomApprenti = $newData; }
    public function setNomApprenti(String $newData) { $this->nomApprenti = $newData; }
    public function setIdEntreprise(int $newData) { $this->idEntreprise = $newData; }
    public function setIdTuteur(int $newData) { $this->idTuteur = $newData; }
    public function setNbRetards(int $newData) { $this->nbRetards = $newData; }
    public function setNbAbsences(int $newData) { $this->nbAbsences = $newData; }

    public function getPrimaryKey() : string { return 'idApprenti'; }
    public function PKIsAutoIncremented() : bool { return true; }
    public function getTableName() : string { return 'Apprenti'; }
}