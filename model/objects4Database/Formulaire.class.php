<?php
class Formulaire implements iManageableObject {
    private $idApprenti;
    private $anneeActuelle;
    private $estPonctuel;
    private $commentaire_estPonctuel;
    private $comportementConforme;
    private $commentaire_compConf;
    private $estMotive;
    private $commentaire_estMotive;
    private $noterPointsImportants;
    private $pointImportant1;
    private $pointImportant2;
    private $acceptePromotion;

    public function __construct(array $data) {
      $this->hydrate($data);
    }

    private function hydrate(array $data) {
        foreach($data as $key => $value) {
        $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getIdApprenti() { return $this->idApprenti; }
    public function getAnneeActuelle() { return $this->anneeActuelle; }
    public function getEstPonctuel() { return $this->estPonctuel; }
    public function getCommentaire_estPonctuel() { return $this->commentaire_estPonctuel; }
    public function getComportementConforme() { return $this->comportementConforme; }
    public function getCommentaire_compConf() { return $this->commentaire_compConf; }
    public function getEstMotive() { return $this->estMotive; }
    public function getCommentaire_estMotive() { return $this->commentaire_estMotive; }
    public function getNoterPointsImportants() { return $this->noterPointsImportants; }
    public function getPointImportant1() { return $this->pointImportant1; }
    public function getPointImportant2() { return $this->pointImportant2; }
    public function getAcceptePromotion() { return $this->acceptePromotion; }

    public function setIdApprenti(int $newData) { if($newData > 0) $this->idApprenti = $newData; }
    public function setAnneeActuelle(int $newData) { $this->anneeActuelle = $newData; }
    public function setEstPonctuel(int $newData) {
        if($newData == 0 || $newData == 1)
            $this->estPonctuel = $newData;
        else
            throw Exception('"estPonctuel" doit valoir vrai(1) ou faux (0)', 101);
    }

    public function setCommentaire_estPonctuel($newData) { if(is_string($newData)) $this->commentaire_estPonctuel = $newData; }
    public function setComportementConforme(int $newData) {
        if($newData == 0 || $newData == 1)
            $this->comportementConforme = $newData;
        else
            throw Exception('"comportementConforme" doit valoir vrai(1) ou faux (0)', 102);
    }

    public function setCommentaire_compConf($newData) { if(is_string($newData)) $this->commentaire_compConf = $newData; }
    public function setEstMotive(int $newData) {
        if($newData == 0 || $newData == 1)
            $this->estMotive = $newData;
        else
            throw Exception('"estMotive" doit valoir vrai(1) ou faux (0)', 103);
    }

    public function setCommentaire_estMotive($newData) { if(is_string($newData)) $this->commentaire_estMotive = $newData; }
    public function setNoterPointsImportants(int $newData) {
        if($newData == 0 || $newData == 1)
            $this->noterPointsImportants = $newData;
        else
            throw Exception('"noterPointsImportants" doit valoir vrai(1) ou faux (0)', 104);
    }

    public function setPointImportant1($newData) { if(is_string($newData)) $this->pointImportant1 = $newData; }
    public function setPointImportant2($newData) { if(is_string($newData)) $this->pointImportant2 = $newData; }
    public function setAcceptePromotion(int $newData) {
        if($newData == 0 || $newData == 1)
            $this->acceptePromotion = $newData;
        else
            throw Exception('"acceptePromotion" doit valoir vrai(1) ou faux (0)', 405);
    }

    public function getPrimaryKey() : string { return 'idApprenti'; }
    public function PKIsAutoIncremented() : bool { return false; }
    public function getTableName() : string { return 'Formulaire'; }
}