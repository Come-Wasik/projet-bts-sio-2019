<?php

class Formateur implements iManageableObject {
    private $idFormateur;
    private $prenom;
    private $nom;
    private $identifiant;
    private $motDePasse;

    public function __construct(array $data) {
        $this->hydrate($data);
      }
  
      private function hydrate(array $data) {
          foreach($data as $key => $value) {
          $method = 'set'.ucfirst($key);
              if(method_exists($this, $method)) {
                  $this->$method($value);
              }
          }
      }

    public function getId() { return $this->idFormateur; }
    public function getPrenom() { return $this->prenom; }
    public function getNom() { return $this->nom; }
    public function getIdentifiant() { return $this->identifiant; }
    public function getMotDePasse() { return $this->motDePasse; }

    public function setId(int $newData) { $this->idFormateur = $newData; }
    public function setPrenom(String $newData) { $this->prenom = $newData; }
    public function setNom(String $newData) { $this->nom = $newData; }
    public function setIdentifiant(String $newData) { $this->identifiant = $newData; }
    public function setMotDePasse(String $newData) { $this->motDePasse = $newData; }

    public function getPrimaryKey() : string { return 'idFormateur'; }
    public function PKIsAutoIncremented() : bool { return true; }
    public function getTableName() : string { return 'Formateur'; }
}