<?php
class Classe implements iManageableObject {
    private $idClasse;
    private $nomClasse;


    public function __construct(array $data) {
      $this->hydrate($data);
    }

    private function hydrate(array $data) {
        foreach($data as $key => $value) {
        $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getIdClasse() { return $this->idClasse; }
    public function getNomClasse() { return $this->nomClasse; }
    
    public function setIdClasse(int $newData) { $this->idClasse = $newData; }
    public function setNomClasse(String $newData) { $this->nomClasse = $newData; }

    public function getPrimaryKey() : string { return 'idClasse'; }
    public function PKIsAutoIncremented() : bool { return true; }
    public function getTableName() : string { return 'classe'; }
}