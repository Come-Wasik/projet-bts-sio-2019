<?php

class ManualManager extends CideRUseDemise {
  // Apprenti
  public function getFirstnameAndLastnameAndReturnIdApprenti($prenomApprenti, $nomApprenti) {
      $request = $this->_db->prepare('SELECT * FROM Apprenti WHERE prenomApprenti = :prenomApprenti && nomApprenti = :nomApprenti');
      $request->bindValue(':prenomApprenti', $prenomApprenti);
      $request->bindValue(':nomApprenti', $nomApprenti);
      $request->execute();
      $data = $request->fetch(PDO::FETCH_ASSOC);
      return $data['idApprenti'];
    }

  public function getApprentiWithMaxAbsence() {
    $request = $this->_db->prepare('SELECT * FROM Apprenti ORDER BY nbAbsences LIMIT 1');
    if($request->execute()) {
      $data = $request->fetch(PDO::FETCH_ASSOC);
      return new Apprenti($data);
    }
    else
      return false;
  }

  public function getApprentiWithMaxLateness() {
    $request = $this->_db->prepare('SELECT * FROM Apprenti ORDER BY nbRetards LIMIT 1');
    if($request->execute()) {
      $data = $request->fetch(PDO::FETCH_ASSOC);
      return new Apprenti($data);
    }
    else
      return false;
  }

  // Classe_has_Apprenti
  public function getIdClasse($idApprenti, $anneeActuelle) {
    $request = $this->_db->prepare('SELECT idClasse FROM Classe_has_Apprenti WHERE idApprenti = :idApprenti && anneeActuelle = :anneeActuelle');
    $request->bindValue(':idApprenti', $idApprenti, PDO::PARAM_INT);
    $request->bindValue(':anneeActuelle', $anneeActuelle, PDO::PARAM_INT);
    $request->execute();
    $data = $request->fetch(PDO::FETCH_ASSOC);
    if(!empty($data))
      return $data['idClasse'];
    else
      return false;
  }

  // Classe
  public function getNomAndReturnIdClasse($nomClasse) {
    $request = $this->_db->prepare('SELECT * FROM Classe WHERE nomClasse = :nomClasse');
    $request->bindValue(':nomClasse', $nomClasse);
    $request->execute();
    $data = $request->fetch(PDO::FETCH_ASSOC);
    return $data['idClasse'];
  }

  // Entreprise
  public function getNomAndReturnIdEntreprise($nomEntreprise) {
    $request = $this->_db->prepare('SELECT * FROM Entreprises WHERE nomEntreprise = :nomEntreprise');
    $request->bindValue(':nomEntreprise', $nomEntreprise);
    $request->execute();
    if(($data = $request->fetch(PDO::FETCH_ASSOC)) != null) {;
      return (int) $data['idEntreprise'];
    }

    else
      return null;
  }

  // Formateur
  public function verifierIdentifiants($identifiant, $motDePasse) {
    $request = $this->_db->prepare('SELECT * FROM Formateur WHERE identifiant = :identifiant && motDePasse = :motDePasse');
    $request->bindValue(':identifiant', $identifiant);
    $request->bindValue(':motDePasse', hash('sha512', $motDePasse));
    $request->execute();

    $data = $request->fetch(PDO::FETCH_ASSOC);
    if(empty($data))
      throw new Exception('L\'identifiant ou le mot de passe est incorrect', 2);
    return $data;
  }

  // Tuteur
  public function getNomAndReturnIdTuteur($nomTuteur) {
    $request = $this->_db->prepare('SELECT * FROM Tuteur WHERE nomTuteur = :nomTuteur');
    $request->bindValue(':nomTuteur', $nomTuteur);
    $request->execute();
    $data = $request->fetch(PDO::FETCH_ASSOC);
    return $data['idTuteur'];
  }
}