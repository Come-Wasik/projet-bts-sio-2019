<?php
class RemplirBDD {    
    private function creerObjFormulaire($donneesFormulaire) {
        $manualManager = new ManualManager(DbMainStarter::useMySqlDb());

        $formulaire = new Formulaire([
            'anneeActuelle' =>          $donneesFormulaire['anneeActuelle'],
            'estPonctuel' =>            $donneesFormulaire['estPonctuel'],
            'comportementConforme' =>   $donneesFormulaire['comportementConforme'],
            'estMotive' =>              $donneesFormulaire['estMotive'],
            'noterPointsImportants' =>  $donneesFormulaire['noterPointsImportants'],
            'acceptePromotion' =>       $donneesFormulaire['acceptePromotion']]);
        
        if(isset($donneesFormulaire['commentaire_estPonctuel']))$formulaire->setCommentaire_estPonctuel(htmlspecialchars($_POST['commentaire_estPonctuel']));
        if(isset($donneesFormulaire['commentaire_compConf']))   $formulaire->setCommentaire_compConf(htmlspecialchars($_POST['commentaire_compConf']));
        if(isset($donneesFormulaire['commentaire_estMotive']))  $formulaire->setCommentaire_estMotive(htmlspecialchars($_POST['commentaire_estMotive']));
        if(isset($donneesFormulaire['pointImportant1']))        $formulaire->setPointImportant1(htmlspecialchars($_POST['pointImportant1']));
        if(isset($donneesFormulaire['pointImportant2']))        $formulaire->setPointImportant2(htmlspecialchars($_POST['pointImportant2']));
        $formulaire->setIdApprenti($manualManager->getFirstnameAndLastnameAndReturnIdApprenti($donneesFormulaire['prenomApprenti'], $donneesFormulaire['nomApprenti']));
        return $formulaire;
    }

    private function creerObjTuteur($donneesFormulaire) {
        $manualManager = new ManualManager(DbMainStarter::useMySqlDb());
        $tuteur = new Tuteur(['nomTuteur' => $donneesFormulaire['nom_tuteur'],
            'telTuteur' =>  $donneesFormulaire['tel_tuteur'],
            'mailTuteur' => strtolower($donneesFormulaire['mailAVerifier']),
            'idEntreprise' => $manualManager->getNomAndReturnIdEntreprise($donneesFormulaire['nom_entreprise'])]);
        return $tuteur;
    }

    private function creerObjApprenti($donneesFormulaire) {
        $manualManager = new ManualManager(DbMainStarter::useMySqlDb());
        $apprenti = new Apprenti(['prenomApprenti' => $donneesFormulaire['prenomApprenti'],
        'nomApprenti' => $donneesFormulaire['nomApprenti'],
        'idEntreprise' => $manualManager->getNomAndReturnIdEntreprise($donneesFormulaire['nom_entreprise']),
        'idTuteur' => $manualManager->getNomAndReturnIdTuteur($donneesFormulaire['nom_tuteur']),
        'nbRetards' => $donneesFormulaire['nbRetards'],
        'nbAbsences' => $donneesFormulaire['nbAbsences']]);
        return $apprenti;
    }

    private function creerObjLien_classe_et_apprenti($donneesFormulaire) {
        $manualManager = new ManualManager(DbMainStarter::useMySqlDb());
        $lien_classe_et_apprenti = new Classe_has_Apprenti(['idClasse' => $manualManager->getNomAndReturnIdClasse($donneesFormulaire['classeApprenti']),
                'idApprenti' => $manualManager->getFirstnameAndLastnameAndReturnIdApprenti($donneesFormulaire['prenomApprenti'], $donneesFormulaire['nomApprenti']),
                'anneeActuelle' => $donneesFormulaire['anneeActuelle']]);
        return $lien_classe_et_apprenti;
    }

    public function UploadFormulaire(array $donneesFormulaire) {
        // On vérifie que le formulaire est remplit avec les données minimales requises
        if(isset($donneesFormulaire['nomApprenti'])
        && isset($donneesFormulaire['prenomApprenti'])
        && isset($donneesFormulaire['classeApprenti'])
        && isset($donneesFormulaire['nom_entreprise'])
        && isset($donneesFormulaire['nom_tuteur'])
        && isset($donneesFormulaire['tel_tuteur'])

        && isset($donneesFormulaire['estPonctuel'])
        && isset($donneesFormulaire['comportementConforme'])
        && isset($donneesFormulaire['estMotive'])
        && isset($donneesFormulaire['noterPointsImportants'])
        && isset($donneesFormulaire['mailAVerifier'])
        && isset($donneesFormulaire['acceptePromotion'])
        && isset($donneesFormulaire['nbRetards'])
        && isset($donneesFormulaire['nbAbsences'])) {
            
            // On force les valeurs à s'adresser correctement pour les rentrer dans la base de données
            $donneesFormulaire['estPonctuel'] = ($donneesFormulaire['estPonctuel'] == 'true' ? 1 : 0);
            $donneesFormulaire['comportementConforme'] = ($donneesFormulaire['comportementConforme'] == 'true' ? 1 : 0);
            $donneesFormulaire['estMotive'] = ($donneesFormulaire['estMotive'] == 'true' ? 1 : 0);
            $donneesFormulaire['noterPointsImportants']  = ($donneesFormulaire['noterPointsImportants'] == 'true' ? 1 : 0);
            $donneesFormulaire['acceptePromotion'] = ($donneesFormulaire['acceptePromotion'] == 'true' ? 1 : 0);

            // On obtient les infos complémentaires au formulaire pour remplir la BDD
            $donneesFormulaire['anneeActuelle'] = (int) date('Y');

            // On enregistre les donnéesdu formulaire
            //$this->donneesFormulaire = $donneesFormulaire;

            // On force le type
            $donneesFormulaire['nbRetards'] = (int) $donneesFormulaire['nbRetards'];
            $donneesFormulaire['nbAbsences'] = (int) $donneesFormulaire['nbAbsences'];

            // On créer nos objets
                // Les managers
            $CideRUseDemise = new CideRUseDemise(DbMainStarter::useMySqlDb());
            $manualManager = new ManualManager(DbMainStarter::useMySqlDb());

            // Les tables de la BDD et on termine
            $classe = new Classe(['nomClasse' => $donneesFormulaire['classeApprenti']]);
            $CideRUseDemise->add($classe);

            $entreprise = new Entreprise(['nomEntreprise' => $donneesFormulaire['nom_entreprise'] ]);
            $CideRUseDemise->add($entreprise);

            $tuteur = $this->creerObjTuteur($donneesFormulaire);
            $CideRUseDemise->add($tuteur);

            $apprenti = $this->creerObjApprenti($donneesFormulaire);
            $CideRUseDemise->add($apprenti);

            $lien_classe_et_apprenti = $this->creerObjLien_classe_et_apprenti($donneesFormulaire);
            $CideRUseDemise->add($lien_classe_et_apprenti);

            $formulaire = $this->creerObjFormulaire($donneesFormulaire);
            $CideRUseDemise->add($formulaire);

            // On enregistre les id des objets créé
            $idObjets['idApprenti'] = $manualManager->getFirstnameAndLastnameAndReturnIdApprenti($apprenti->getPrenomApprenti(), $apprenti->getNomApprenti());
            $idObjets['idClasse'] = $manualManager->getIdClasse($idObjets['idApprenti'], $donneesFormulaire['anneeActuelle']);
            $idObjets['idEntreprise'] = $apprenti->getIdEntreprise();
            $idObjets['idTuteur'] = $apprenti->getIdTuteur();
            $idObjets['idformulaire'] = $idObjets['idApprenti'];

            // On les renvoit
            return $idObjets;
        }
        else {
            throw new Exception('Formulaire incomplet', 5);
        }
    }


    /* DEBUG
       ----- */
       
    public function UploadFormulaireWithDebug(array $donneesFormulaire) {
        // On vérifie que le formulaire est remplit avec les données minimales requises
        if(isset($donneesFormulaire['nomApprenti'])
        && isset($donneesFormulaire['prenomApprenti'])
        && isset($donneesFormulaire['classeApprenti'])
        && isset($donneesFormulaire['nom_entreprise'])
        && isset($donneesFormulaire['nom_tuteur'])
        && isset($donneesFormulaire['tel_tuteur'])

        && isset($donneesFormulaire['estPonctuel'])
        && isset($donneesFormulaire['comportementConforme'])
        && isset($donneesFormulaire['estMotive'])
        && isset($donneesFormulaire['noterPointsImportants'])
        && isset($donneesFormulaire['mailAVerifier'])
        && isset($donneesFormulaire['acceptePromotion'])
        && isset($donneesFormulaire['nbRetards'])
        && isset($donneesFormulaire['nbAbsences'])) {

            // On force les valeurs à s'adresser correctement pour les rentrer dans la base de données
            $donneesFormulaire['estPonctuel'] = ($donneesFormulaire['estPonctuel'] == 'true' ? 1 : 0);
            $donneesFormulaire['comportementConforme'] = ($donneesFormulaire['comportementConforme'] == 'true' ? 1 : 0);
            $donneesFormulaire['estMotive'] = ($donneesFormulaire['estMotive'] == 'true' ? 1 : 0);
            $donneesFormulaire['noterPointsImportants']  = ($donneesFormulaire['noterPointsImportants'] == 'true' ? 1 : 0);
            $donneesFormulaire['acceptePromotion'] = ($donneesFormulaire['acceptePromotion'] == 'true' ? 1 : 0);

            // On obtient les infos complémentaires au formulaire pour remplir la BDD
            $donneesFormulaire['anneeActuelle'] = (int) date('Y');

            //$this->donneesFormulaire = $donneesFormulaire;

            // On créer nos objets
                // Les managers
                $CideRUseDemise = new CideRUseDemise(DbMainStarter::useMySqlDb());
                $manualManager = new ManualManager(DbMainStarter::useMySqlDb());
    
                // Les tables de la BDD et on termine
                $classe = new Classe(['nomClasse' => $donneesFormulaire['classeApprenti']]);
                $CideRUseDemise->add($classe);
    
                $entreprise = new Entreprise(['nomEntreprise' => $donneesFormulaire['nom_entreprise'] ]);
                $CideRUseDemise->add($entreprise);
    
                $tuteur = $this->creerObjTuteur($donneesFormulaire);

                $CideRUseDemise->add($tuteur);
    
                $apprenti = $this->creerObjApprenti($donneesFormulaire);
                $CideRUseDemise->add($apprenti);
    
                $lien_classe_et_apprenti = $this->creerObjLien_classe_et_apprenti($donneesFormulaire);
                $CideRUseDemise->add($lien_classe_et_apprenti);
    
                $formulaire = $this->creerObjFormulaire($donneesFormulaire);
                $CideRUseDemise->add($formulaire);
            require 'public/#test/debugFormulaire.php';
        }
        else {
            throw new Exception('Formulaire incomplet', 5);
        }
    }
}