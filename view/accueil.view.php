<?php $titreDuDocument = 'TP3' ?>

<?php $header = 'Accueil' ?>

<?php ob_start(); ?>
<h1>Bienvenue à l'accueil!</h1>
<p>
    <?php if($_SESSION['user']['connected'] ?? false)  { ?>
        Vous êtes connecté! Pour continuer <a href="index.php?act=demandeEnvoyerMail"> Cliquez ici !</a>
    <?php }
    else { ?>
        Vous êtes un utilisateur? <a href="index.php?act=demandeConnexion">Identifiez vous !</a>
    <?php } ?>
</p>
<?php $content = ob_get_clean(); ?>

<?php require '_template.view.php'; ?>