<?php $titreDuDocument = 'TP3' ?>

<?php $header = 'Plus grand nombre d\'absences et plus grand nombre de retards' ?>

<?php ob_start(); ?>
<?php
//aff_r($apprentiPlusRetards);
//aff_r($apprentiPlusAbsences);
?>
<p>L'apprenti avec le plus de retards en accumule <?= $apprentiPlusRetards->getNbRetards() ?> et se nomme
    <?= $apprentiPlusRetards->getPrenomApprenti() ?> <?= $apprentiPlusRetards->getNomApprenti() ?></p>

<p>L'apprenti avec le plus d'absences en accumule <?= $apprentiPlusAbsences->getNbAbsences() ?> et se nomme
    <?= $apprentiPlusAbsences->getPrenomApprenti() ?> <?= $apprentiPlusAbsences->getNomApprenti() ?></p>
<?php $content = ob_get_clean(); ?>

<?php require '_template.view.php'; ?>