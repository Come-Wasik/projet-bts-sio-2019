<?php $titreDuDocument = 'TP3' ?>

<?php $header = 'Page de connexion' ?>

<?php ob_start(); ?>
<form method="post" action="index.php?act=demandeConnexion" id="login">
    <fieldset>
        <div class="element">
            <label>Nom d'utilisateur : </label>
            <input type="text" name="nomUtilisateur" required="required" />
        </div>
        <div class="element">
            <label>Mot de passe : </label>
            <input type="password" name="password" required="required" />
        </div>
        <footer class="element">
            <input type="submit" value="Confirmer" />
            <input type="reset" />
        </footer>
        <?php
        if(($messageErreur ?? null)) {?>
            Erreur : <?= $messageErreur ?>
        <?php } ?>
    </fieldset>
</form>
<?php $content = ob_get_clean(); ?>

<?php require '_template.view.php'; ?>