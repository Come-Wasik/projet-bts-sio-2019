<?php $titreDuDocument = 'TP3' ?>

<?php $header = 'Envoyer un mail à un tuteur' ?>

<?php ob_start(); ?>
<p class="alignCenter">
        <a href="index.php?act=deconnexion" id="deconnexion" >Se déconnecter</a>
</p>
<p> <a href="?act=plusGrandNombre">Cliquez ici</a> pour voir le plus grand nombre d'absences et de retard</p>
<form method="post" action="index.php?act=envoyerMailFormulaire">
    <label for="mailDestinataire">Envoyer un mail à :</label>
    <input type="email" name="mailDestinataire" id="mailDestinataire" required="required"
            placeholder="name@domain.ext" pattern="^[A-Za-z_.-]+@[A-Za-z]+\.[A-Za-z]{2,4}$" style="width: 170px;" /></li>
    <input type="submit" />
</form>
<?php if(isset($messageUtilisateur)) echo '<strong>', $messageUtilisateur, '</strong>'; ?>
<?php $content = ob_get_clean(); ?>

<?php require '_template.view.php'; ?>