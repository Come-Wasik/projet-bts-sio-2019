<?php $titreDuDocument = 'TP3' ?>

<?php $header = 'ENTRETIENT DE DÉBUT DE CONTRAT 2016-2017' ?>

<?php ob_start(); ?>
<form method="post" action="index.php?act=enregistrerFormulaire">
    <?php require '_renseignements.view.php'; ?>
    <?php require '_questionnaire.view.php'; ?>
    <footer>
        <input type="submit" value="Confirmer" />
        <input type="reset" value="Relancer" />
    </footer>
</form>
<?php $content = ob_get_clean(); ?>

<?php require 'view/_template.view.php'; ?>