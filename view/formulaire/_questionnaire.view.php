<main id="questionnaire">
    <section id="presentation">
        <header>
            <h3>PRESENTER LA DEMARCHE RELATION ENTREPRISE DU CFA UTEC</h3>
            <p>Le CFA UTEC s'attache à une bonne intégration du jeune en entreprise ansi qu'à sa progression
                pédagogique afin que nous puissions ensemble l'accompagner dans la réussite de son diplôme.</p>
        </header>

        <main>
            <p>Questionnaire :</p>

            <section class="question">
                <p>
                    <span class="blue">Q1 :</span> <span class="afficherNomEtPrenom">X</span> est-il ponctuel ?
                    <input type="radio" name="estPonctuel" id="estPonctuelTrue" value="true" onclick="enableEstPonctuelCom()" checked="checked" />
                        <label for="estPonctuelTrue">Oui</label>
                    <input type="radio" name="estPonctuel" id="estPonctuelFalse" value="false" onclick="disableEstPonctuelCom()" />
                        <label for="estPonctuelFalse">Non</label>
                </p>
                <p>
                    <label for="commentaire_estPonctuel">Commentaire :</label>
                    <br />
                    <textarea name="commentaire_estPonctuel" id="commentaire_estPonctuel" placeholder="Mon commentaire..."></textarea>
                </p>
            </section>

            <section class="question">
                <p>
                    <span class="blue">Q2 :</span> <span class="afficherNomEtPrenom">X</span> a-t-il un comportement conforme à vos attentes ?
                    <br /> <em>(Communicatif, souriant, curieux, observateur, à l'aise dans l'équipe ...)</em>

                    <input type="radio" name="comportementConforme" id="comportementConformeTrue" value="true" oninput="enableCompConfCom()" checked="checked" />
                        <label for="comportementConformeTrue">Oui</label>
                    <input type="radio" name="comportementConforme" id="comportementConformeFalse" value="false" oninput="disableCompConfCom()" />
                        <label for="comportementConformeFalse">Non</label>
                </p>
                <p>
                    <label for="commentaire_compConf">Commentaire :</label>
                    <br />
                    <textarea name="commentaire_compConf" id="commentaire_compConf" placeholder="Mon commentaire..."></textarea>
                </p>
            </section>

            <section class="question">
                <p>
                    <span class="blue">Q3 :</span> <span class="afficherNomEtPrenom">X</span> vous semble-t-il motivé pour le métier ?
                    <input type="radio" name="estMotive" id="estMotiveTrue" value="true" oninput="enableEstMotiveCom()" checked="checked" />
                        <label for="estMotiveTrue">Oui</label>
                    <input type="radio" name="estMotive" id="estMotiveFalse" value="false" oninput="disableEstMotiveCom()" />
                        <label for="estMotiveFalse">Non</label>
                </p>
                <p>
                    <label for="commentaire_estMotive">Commentaire :</label>
                    <br />
                    <textarea name="commentaire_estMotive" id="commentaire_estMotive" placeholder="Mon commentaire..."></textarea>
                </p>
            </section>

            <section class="commentaire" >

                <p><i>Si oui aux 3 questions, RAS</i></p>
                <p><i>Si non à 1 question (Q1-Q2-Q3) :</i></p>

                <ul>
                    <li>Très bien, Madame/Monsieur, on a bien pris note.</li>
                    <li>L'équipe pédagogique du CFA UTEC rencontrera de nouveau votre apprenti(e) pour traiter ce(s) point(s) d'amélioration de début de formation.</li>
                    <li>Le retour vous sera fait par l'intermédiaire du carnet de liaison.</li>
                </ul>
            </section>

            <section class="question">
                <p>
                    <span class="blue">Q4 :</span> Voyez-vous des points particuliers, en ce début de formation, sur lequels vous souhaitez <br/>
                    que nous intervenions au CFA ?
                    <input type="radio" name="noterPointsImportants" id="noterPointsImportantsTrue" value="true" onclick="enableImportantPointCom()" />
                        <label for="noterPointsImportantsTrue">Oui</label>
                    <input type="radio" name="noterPointsImportants" id="noterPointsImportantsFalse" value="false" onclick="disableImportantPointCom()" checked="checked" />
                        <label for="noterPointsImportantsFalse">Non</label>
                </p>
            </section>

            <section class="question">
                <p>
                    <span class="blue">Q5 :</span> <em>Si oui, prise de commande(s) : 1 ou 2 points précis sur lesquels ont peut facilement agir <br />
                    au CFA  en début de contrat.</em> Point(s) à améliorer :
                </p>
                <p>
                    <label for="pointImportant1">Point 1 :</label>
                    <br />
                    <textarea name="pointImportant1" id="pointImportant1" value="true" placeholder="Mon commentaire..."></textarea>
                </p>
                <p>
                    <label for="pointImportant2">Point 2 :</label>
                    <br />
                    <textarea name="pointImportant2" id="pointImportant2" value="false" placeholder="Mon commentaire..."></textarea>
                </p>
            </section>

            <p>Le retour vous sera fait par l'intermédiaire du carnet de liaison.</p>
        </main>
    </section>

    <section id="conclusion">
        <header>
            <h3>CONCLURE SUR LA CONTINUITE DU LIEN CFA-ENTREPRISE</h3>
            <p>Nous aurons l'occasion d'avoir différents échanges tout au long de cette année :</p>
            <ul>
                <li>Aux entretients de début de contrat qui se dérouleront au CFA (date : <?= $date1 ?>)</li>
                <li>Lors du petit déjeuner tuteurs (date : <?= $date2 ?>)</li>
                <li>Régulièrement, à travers le carnet de liaison,</li>
                <li>Et bien entendu lors de notre visite tutorat en entreprise,</li>
            </ul>
        </header>

        <main>
            <p>On reste à votre disposition (mail et tél du PP)</p>

            <p>On vérifie votre adresse mail et ligne directe</p>
            <ul>
                <li><label for="mailAVerifier">Mail : </label>
                    <input type="text" name="mailAVerifier" id="mailAVerifier" required="required"
                        placeholder="name@domain.ext" pattern="^[a-z_.-]+@[a-z]+\.[a-z]{2,4}$" /></li>
                <li><label for="telephoneAVerifier">Tel : </label>
                    <input type="text" name="telephoneAVerifier" id="telephoneAVerifier" pattern="^[A-Za-z0-9]+$"
                            required="required" disabled="disabled" /></li>
            </ul>
            
            <section class="question">
                <p>
                    <span class="blue">Q6 :</span> Le Service Promotion du CFA UTEC vous informe que nous avons des profils<br />
                    intéressants de jeunes à vous présenter, dans le cas où vous auriez de nouveaux besoins<br />
                    d'apprentis pour cette année. Souhaitez-vous qu'un conseillers vous rappelle à ce sujet ?<br />
                    <input type="radio" name="acceptePromotion" id="acceptePromotionTrue" value="true" checked="checked" />
                        <label for="acceptePromotionTrue">Oui</label>
                    <input type="radio" name="acceptePromotion" id="acceptePromotionFalse" value="false" />
                        <label for="acceptePromotionFalse">Non</label>
                </p>
            </section>

            <section class="question">
                <p>
                    <span class="blue">Q7 :</span> Notez ses retards et absences<br />
                    <label for="nbRetards">Retards :</label>
                        <input type="text" name="nbRetards" id="nbRetards" required="required" />
                    <label for="nbAbsences">Absences :</label>
                        <input type="text" name="nbAbsences" id="nbAbsences" required="required" /> 
                </p>
            </section>
        </main>
    </section>
</main>