<header id="renseignements">
    <section class="form_line">
        <section>
            <label for="prenomApprenti">Prénom de l'apprenti :</label>
            <input type="text" name="prenomApprenti" id="prenomApprenti" required="required"
                oninput="changeNames()" pattern="^[A-Za-zéèç]+$" />
        </section>
        <section>
            <label for="nomApprenti">Nom de l'apprenti :</label>
            <input type="text" name="nomApprenti" id="nomApprenti" required="required"
                oninput="changeNames()" pattern="^[A-Za-zéèç]+$" />
        </section>
    </section>

    <section class="form_line">
        <section>
            <label for="classeApprenti">Classe de l'apprenti :</label>
            <input type="text" name="classeApprenti" id="classeApprenti" required="required" pattern="^[0-9A-Za-z _-]+$" />
        </section>
        <section>
            <label for="nom_entreprise">Nom de l'entreprise :</label>
                <input type="text" name="nom_entreprise" id="nom_entreprise" required="required" pattern="^[0-9A-Za-z _-]+$" />
        </section>
    </section>

    <section class="form_line">
        <label for="nom_tuteur">Nom du Tuteur :</label>
            <input type="text" name="nom_tuteur" id="nom_tuteur" required="required" pattern="^[A-Za-zéè]+$" placeholder="Votre nom" />
    </section>

    <section class="form_line">
        <label for="tel_tuteur">Tél. Tuteur :</label>
            <input type="text" name="tel_tuteur" id="tel_tuteur" oninput="changerChampTelephonePourConclure()"
                pattern="[0-9]{10}" title="Veuillez inscrire un numéro à 10 chiffres" placeholder="0611223344" required="required" />
    </section>

    <img src="public/images/deco.png" alt="vagues" id="vagues" />
</header>