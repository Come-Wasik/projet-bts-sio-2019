<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta author="Côme Wasik et Fabien Albares" />
        <meta charset="UTF-8" />
        <title><?= $titreDuDocument ?></title>

        <link rel="stylesheet" href="public/css/main.css" />
        <script src="public/js/main.js" type="text/javascript" ></script>
    </head>
    <body onload="onstart()">
        <section id="page_content">
            <header>
                <h1 id="en_tete">CFA UTEC - Département Pédagogique</h1>
                <section id="encadre">
                    <img src="public/images/UTEC.png" alt="utec" id="logo" />
                    <h2 id="titre_bleu"><?= $header ?></h2>
                </section>
            </header>
            <main>
                <noscript>Pour profiter pleinement des fonctionnalitées du site. Veuillez utiliser un navigateur comprenant  le Javascript.</noscript>
                <?= $content ?>
            </main>
        </section>
    </body>
</html>
