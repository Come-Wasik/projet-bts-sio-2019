<style>
    .header #utec_tag {
        width: 45%;
        padding: 7px;

        margin-left: 400px;
        margin-bottom: -1px;

        text-align: center;
        font-size: 18px;
        color: white;
        background-color: rgb(255, 86, 56);
        border: black 1px solid;
        border-radius: 2px;
    }

    .header #titre_tag { border-collapse: collapse; }
    #titre_tag td { border: 1px solid black; }
 
    #titre_tag img { width: 220px; }

    #titre_tag .img { width: 25%; }

    #titre_tag .title {
        width: 70%;
        vertical-align: middle;
        text-align: center;

        font-size: 21px;
        color: rgb(89, 92, 255);
        font-weight: bold;
        background-color: rgb(0,230,200);
    }

    .body {
        margin-top: 50px;
        margin-left: 20px;
    }

    .body h1 {
        display: block;
        padding: 10px;
        color: red;
        font-size: 20px;

        border: rgb(0,210,200) 1px solid;
        margin-bottom: 5px;
    }

    .body .ensemble {
        display: block;
        margin: 10px;
        margin-left: 30px;
    }

    .ensemble .ligne {
        display: block;
        margin-bottom: 4px;
    }

    .ensemble .question {
        font-weight: bold;
    }

    .ensemble .reponse {
        margin-top: 5px;
        margin-left: 30px;
    }
</style>