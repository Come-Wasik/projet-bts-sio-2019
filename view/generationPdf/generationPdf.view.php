<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta author="Côme Wasik et Fabien Albares" />
        <meta charset="UTF-8" />
        <title>TP3</title>
        <?php require __DIR__.'/style.php'; ?>
    </head>
    <body>
        <div class="header">
            <div id="utec_tag">CFA UTEC - Département Pédagogique</div>
            <table id="titre_tag">
                <tr>
                    <td class="img"><img src="public/images/UTEC.png" alt="utec" id="logo" /></td>
                    <td class="title">ENTRETIENT DE DÉBUT DE CONTRAT 2019-2020</td>
                </tr>
            </table>
        </div>
        <div class="body">
            <h1>Données de l'apprenti</h1>
            <div class="ensemble">
                <div class="ligne"><strong>Prénom de l'apprenti :</strong> <?= $apprenti->getPrenomApprenti(); ?></div>
                <div class="ligne"><strong>Nom de l'apprenti :</strong> <?= $apprenti->getNomApprenti(); ?></div>
                <div class="ligne"><strong>Classe de l'apprenti :</strong> <?= $classe->getNomClasse(); ?></div>
                <div class="ligne"><strong>Entreprise de l'apprenti :</strong> <?= $entreprise->getNomEntreprise(); ?></div>
                <div class="ligne"><strong>Tuteur de l'apprenti :</strong> Mr/Mme <?= $tuteur->getNomTuteur(); ?></div>
                <div class="ligne"><strong>Téléphone du tuteur :</strong> <?= $tuteur->getTelTuteur(); ?></div>
            </div>
            <h1>Réponses au questionnaire :</h1>
            <div class="ensemble">
                <div class="question">Q1 : L'apprenti est-il ponctuel ?</div>
                <span class="reponse"><?php if($formulaire->getEstPonctuel()) echo 'Oui : '; else echo 'Non'; ?></span>
                <span class="reponse">
                    <?php if($formulaire->getCommentaire_estPonctuel() ?? false) { ?>
                        <?= $formulaire->getCommentaire_estPonctuel(); ?>
                    <?php } ?>
                </span>
            </div>

            <div class="ensemble">
                <div class="question">Q2 : L'apprenti a-t-il un comportement conforme à vos attentes ?<br />
                    <em>(Communicatif, souriant, curieux, observateur, à l'aise dans l'équipe ...)</em></div>
                <span class="reponse"><?php if($formulaire->getComportementConforme()) echo 'Oui : '; else echo 'Non'; ?></span>
                <span class="reponse">
                    <?php if($formulaire->getCommentaire_compConf() ?? false) { ?>
                        <?= $formulaire->getCommentaire_compConf(); ?>
                    <?php } ?>
                </span>
            </div>

            <div class="ensemble">
                <div class="question">Q3 : L'apprenti vous semble-t-il motivé pour le métier ?</div>
                <span class="reponse"><?php if($formulaire->getEstMotive()) echo 'Oui : '; else echo 'Non'; ?></span>
                <span class="reponse">
                    <?php if($formulaire->getCommentaire_estMotive() ?? false) { ?>
                        <?= $formulaire->getCommentaire_estMotive(); ?>
                    <?php } ?>
                </span>
            </div>

            <div class="ensemble">
                <div class="question">Avez vous noté des points particulier ?</div>
                <div class="reponse"><?php if($formulaire->getNoterPointsImportants()) echo 'Oui'; else echo 'Non'; ?></div>
                <?php if($formulaire->getPointImportant1() ?? false) { ?>
                    <div class="reponse"> <?= $formulaire->getPointImportant1(); ?> </div>
                <?php } ?>
                <?php if($formulaire->getPointImportant2() ?? false) { ?>
                    <div class="reponse"> <?= $formulaire->getPointImportant2(); ?> </div>
                <?php } ?>
            </div>

            <div class="ensemble">
                <div class="question">Rester informé avec des propositions d'apprentis par email?</div>
                <div class="reponse"><?php if($formulaire->getAcceptePromotion()) echo 'Oui '; else echo 'Non'; ?></div>
            </div>

            <div class="ensemble">
                <div class="question">Nombre de retards</div>
                <div class="reponse"><?= $apprenti->getNbRetards(); ?></div>
                <div class="question">Nombre d'absences</div>
                <div class="reponse"><?= $apprenti->getNbAbsences(); ?></div>
            </div>
        </div>
    </body>
</html>

