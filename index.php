<?php
/*--------------------------------------
  | Developped by Côme Wasik / nolikein |
  -------------------------------------*/
  /*-----------------
  | About this file |
  -------------------
        Description :
        -------------
            This file is main file and the router in the architecture MVC
*/
try {
    require_once(__DIR__.'/iniProject.php');

    $spikey = new SpikeyLoader('accueil', SpikeyPage::STATIC_PAGE);
    // Front
        $spikey->AddPage(new SpikeyPage('presenterFormulaire', SpikeyPage::DYNAMIC_PAGE));
        $spikey->AddPage(new SpikeyPage('generationPdf', SpikeyPage::DYNAMIC_PAGE, function() {
            if(!isset($_SESSION['idObjetsFormulaire']))
                reloadToDefaultPage();
        }));
        $spikey->AddPage(new SpikeyPage('demandeConnexion', SpikeyPage::DYNAMIC_PAGE, function() {
            if($_SESSION['user']['connected'] ?? false)
                reloadToDefaultPage();
        }));
        $spikey->AddPage(new SpikeyPage('demandeEnvoyerMail', SpikeyPage::DYNAMIC_PAGE, function() {
            if(!$_SESSION['user']['connected'] ?? false)
                reloadToDefaultPage();
        }));
        //$spikey->AddPage(new SpikeyPage('debug', SpikeyPage::DYNAMIC_PAGE));

    // Back
        $spikey->AddPage(new SpikeyPage('enregistrerFormulaire', SpikeyPage::DYNAMIC_PAGE, function() {
            if(empty($_POST))
                reloadToDefaultPage();
        }));
        $spikey->AddPage(new SpikeyPage('envoyerMailFormulaire', SpikeyPage::DYNAMIC_PAGE, function() {
            if(!($_SESSION['user']['connected'] ?? false) || !($_POST['mailDestinataire'] ?? false))
                reloadToDefaultPage();
        }));
        $spikey->AddPage(new SpikeyPage('deconnexion', SpikeyPage::DYNAMIC_PAGE));
        $spikey->AddPage(new SpikeyPage('plusGrandNombre', SpikeyPage::DYNAMIC_PAGE));
    $spikey->start();

} catch(Exception $except) {
    echo '<script type="text/javascript"> alert("Erreur numéro : ',
        $except->getCode(), '\\n ', $except->getMessage(), '"); </script>';
}