-- MySQL Script generated by MySQL Workbench
-- Wed Feb 13 20:39:25 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema form2PDF
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema form2PDF
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `form2PDF` DEFAULT CHARACTER SET utf8 ;
USE `form2PDF` ;

-- -----------------------------------------------------
-- Table `form2PDF`.`Entreprise`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `form2PDF`.`Entreprises` ;

CREATE TABLE IF NOT EXISTS `form2PDF`.`Entreprises` (
  `idEntreprise` INT NOT NULL AUTO_INCREMENT,
  `nomEntreprise` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEntreprise`),
  UNIQUE INDEX `nomEntreprise_UNIQUE` (`nomEntreprise` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `form2PDF`.`Tuteur`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `form2PDF`.`Tuteur` ;

CREATE TABLE IF NOT EXISTS `form2PDF`.`Tuteur` (
  `idTuteur` INT NOT NULL AUTO_INCREMENT,
  `nomTuteur` VARCHAR(45) NOT NULL,
  `telTuteur` VARCHAR(11) NOT NULL,
  `mailTuteur` VARCHAR(45) NOT NULL,
  `idEntreprise` INT NOT NULL,
  PRIMARY KEY (`idTuteur`),
  UNIQUE INDEX `nomTuteur_UNIQUE` (`nomTuteur` ASC),
  INDEX `fk_Tuteurs_Entreprises1_idx` (`idEntreprise` ASC),
  CONSTRAINT `fk_Tuteurs_Entreprises1`
    FOREIGN KEY (`idEntreprise`)
    REFERENCES `form2PDF`.`Entreprises` (`idEntreprise`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `form2PDF`.`Apprenti`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `form2PDF`.`Apprenti` ;

CREATE TABLE IF NOT EXISTS `form2PDF`.`Apprenti` (
  `idApprenti` INT NOT NULL AUTO_INCREMENT,
  `prenomApprenti` VARCHAR(45) NOT NULL,
  `nomApprenti` VARCHAR(45) NOT NULL,
  `idEntreprise` INT NOT NULL,
  `idTuteur` INT NOT NULL,
  `nbRetards` INT NOT NULL,
  `nbAbsences` INT NOT NULL,
  PRIMARY KEY (`idApprenti`),
  INDEX `fk_Apprentis_Entreprises1_idx` (`idEntreprise` ASC),
  INDEX `fk_Apprentis_Tuteurs1_idx` (`idTuteur` ASC),
  UNIQUE INDEX `uq_Apprentis1_uq` (`prenomApprenti` ASC, `nomApprenti` ASC, `idTuteur` ASC),
  CONSTRAINT `fk_Apprentis_Entreprises1`
    FOREIGN KEY (`idEntreprise`)
    REFERENCES `form2PDF`.`Entreprises` (`idEntreprise`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Apprentis_Tuteurs1`
    FOREIGN KEY (`idTuteur`)
    REFERENCES `form2PDF`.`Tuteur` (`idTuteur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `form2PDF`.`Formulaire`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `form2PDF`.`Formulaire` ;

CREATE TABLE IF NOT EXISTS `form2PDF`.`Formulaire` (
  `idApprenti` INT NOT NULL,
  `anneeActuelle` SMALLINT NOT NULL,
  `estPonctuel` TINYINT NOT NULL,
  `commentaire_estPonctuel` VARCHAR(100) NULL,
  `comportementConforme` TINYINT NOT NULL,
  `commentaire_compConf` VARCHAR(100) NULL,
  `estMotive` TINYINT NOT NULL,
  `commentaire_estMotive` VARCHAR(100) NULL,
  `noterPointsImportants` TINYINT NOT NULL,
  `pointImportant1` VARCHAR(100) NULL,
  `pointImportant2` VARCHAR(100) NULL,
  `acceptePromotion` TINYINT NOT NULL,
  PRIMARY KEY (`idApprenti`, `anneeActuelle`),
  CONSTRAINT `fk_Formulaires_Apprentis1`
    FOREIGN KEY (`idApprenti`)
    REFERENCES `form2PDF`.`Apprenti` (`idApprenti`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `form2PDF`.`Classe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `form2PDF`.`Classe` ;

CREATE TABLE IF NOT EXISTS `form2PDF`.`Classe` (
  `idClasse` INT NOT NULL AUTO_INCREMENT,
  `nomClasse` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idClasse`),
  UNIQUE INDEX `nomClasse_UNIQUE` (`nomClasse` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `form2PDF`.`Classe_has_Apprenti`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `form2PDF`.`Classe_has_Apprenti` ;

CREATE TABLE IF NOT EXISTS `form2PDF`.`Classe_has_Apprenti` (
  `idTable` INT NOT NULL AUTO_INCREMENT,
  `idClasse` INT NOT NULL,
  `idApprenti` INT NOT NULL,
  `anneeActuelle` SMALLINT NOT NULL,
  PRIMARY KEY (`idTable`),
  INDEX `fk_Classe_has_Apprentis_Classes1_idx` (`idClasse` ASC),
  INDEX `fk_Classe_has_Apprentis_Apprentis1_idx` (`idApprenti` ASC),
  UNIQUE INDEX `uq_Classe_has_Apprentis1_uq` (`idClasse` ASC, `idApprenti` ASC, `anneeActuelle` ASC) ,
  CONSTRAINT `fk_Classe_has_Apprentis_Classes1`
    FOREIGN KEY (`idClasse`)
    REFERENCES `form2PDF`.`Classe` (`idClasse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Classe_has_Apprentis_Apprentis1`
    FOREIGN KEY (`idApprenti`)
    REFERENCES `form2PDF`.`Apprenti` (`idApprenti`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `form2PDF`.`Formateur`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `form2PDF`.`Formateur` ;

CREATE TABLE IF NOT EXISTS `form2PDF`.`Formateur` (
  `idFormateur` INT NOT NULL AUTO_INCREMENT,
  `prenom` VARCHAR(45) NOT NULL,
  `nom` VARCHAR(45) NOT NULL,
  `identifiant` VARCHAR(45) NOT NULL,
  `motDePasse` VARCHAR(129) NOT NULL,
  PRIMARY KEY (`idFormateur`),
  UNIQUE INDEX `identifiant_UNIQUE` (`identifiant` ASC) )
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
