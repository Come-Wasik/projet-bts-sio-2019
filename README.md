# projetBtsSioSlam
This is my project of BTS SIO SLAM
It's named "form2pdf"

With it, I can :
  + If I am an instructor
    + Connect me on the website (password are hashed)
    + See the max number of absences and delays with the student data
    + Send the link of the form by email (to a apprenticeship manager)
  + If I am an apprenticeship manager
    + Fill it and send the form at a MySql dabatase (named "form2pdf")
    + Have the result by Pdf