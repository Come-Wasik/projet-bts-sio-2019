<?php
use Spipu\Html2Pdf\Html2Pdf;

function pagePresenterFormulaire() {
    // Récupération des dates contenu dans un fichier
    $lines = get2LineOfFile('dates.txt');
    $date1 = $lines[0];
    $date2 = $lines[1];
    
    require SpikeyLoader::getViewDIR().'formulaire/formulaire.view.php';
}

function pageDemandeConnexion() {
    try {

        $infosUtilisateur = ($_POST ?? null);

        if(!empty($infosUtilisateur)) {
            $nomUtilisateur = (String) htmlspecialchars(strtolower($infosUtilisateur['nomUtilisateur']));
            $motDePasse = (String) htmlspecialchars($infosUtilisateur['password']);

            $manualManager = new ManualManager(DbMainStarter::useMySqlDb());
            $informations = $manualManager->verifierIdentifiants($nomUtilisateur, $motDePasse);

            $_SESSION['user']['connected'] = true;
            $_SESSION['user']['identifiant'] = $informations['identifiant'];
            $_SESSION['user']['nom'] = $informations['nom'];
            $_SESSION['user']['prenom'] = $informations['prenom'];
            header('Location: index.php?act=demandeEnvoyerMail');
        }
    }
    catch(Exception $erreurIdentification) {
        $messageErreur = $erreurIdentification->getMessage();
    }
    finally {
        require SpikeyLoader::getViewDIR().'demandeConnexion.view.php';
    }
}

function pageDemandeEnvoyerMail() {
    if($mailEstBienEnvoye = ($_SESSION['mailIsSend'] ?? false)) {
        $messageUtilisateur = 'L\'envoit a fonctionné';
        $_SESSION['mailIsSend'] = null;
    }
    require SpikeyLoader::getViewDIR().'demandeEnvoyerMail.view.php';
}

function pageGenerationPdf() {
    $lines = get2LineOfFile('dates.txt');
    $date1 = $lines[0];
    $date2 = $lines[1];
    
    $CideRUseDemise = new CideRUseDemise(DbMainStarter::useMySqlDb());

    foreach($_SESSION['idObjetsFormulaire'] as &$element) {
        $element = (int) $element;
    }

    $classe = $CideRUseDemise->get(['idClasse' => $_SESSION['idObjetsFormulaire']['idClasse']], 'Classe');
    $entreprise = $CideRUseDemise->get(['idEntreprise' => $_SESSION['idObjetsFormulaire']['idEntreprise']], 'Entreprise');
    $tuteur = $CideRUseDemise->get(['idTuteur' => $_SESSION['idObjetsFormulaire']['idTuteur']], 'Tuteur');
    $apprenti = $CideRUseDemise->get(['idApprenti' => $_SESSION['idObjetsFormulaire']['idApprenti']], 'Apprenti');
    $formulaire = $CideRUseDemise->get(['idApprenti' => $_SESSION['idObjetsFormulaire']['idformulaire']], 'Formulaire');

    ob_start();
        require SpikeyLoader::getViewDIR().'generationPdf/generationPdf.view.php';
    $content = ob_get_clean();
    
    try {
        
        $html2pdf = new Html2Pdf();
        //$html2pdf->setTitle('Formulaire');
        $html2pdf->writeHTML($content);
        $html2pdf->output();
    } catch (Html2PdfException $e) {
        die($e);
    }
    
}

function pageDebug() {
    require SpikeyLoader::getViewDIR().'../public/debug/debugMode.view.php';
}

function pagePlusGrandNombre() {
    $CideRUseDemise = new CideRUseDemise(DbMainStarter::useMySqlDb());
    $apprentisList = $CideRUseDemise->getList('Apprenti');
    
    // Version avec le CRUD autonome
    $nbMaxRetards = 0;
    $nbMaxAbsence = 0;
    $apprentiPlusRetards;
    $apprentiPlusAbsences;

    foreach($apprentisList as $apprenti) {
        if($apprenti->getNbRetards() > $nbMaxRetards) {
            $nbMaxRetards = $apprenti->getNbRetards();
            $apprentiPlusRetards = $apprenti;
        }
    }

    foreach($apprentisList as $apprenti) {
        if($apprenti->getNbAbsences() > $nbMaxAbsence) {
            $nbMaxAbsence = $apprenti->getNbAbsences();
            $apprentiPlusAbsences = $apprenti;
        }
    }

    // Version avec la requête SQL
    /*
    $manualManager = new ManualManager(DbMainStarter::useMySqlDb());
    $apprentiPlusRetards = $manualManager->getApprentiWithMaxLateness();
    $apprentiPlusAbsences = $manualManager->getApprentiWithMaxAbsence();
    */

    require SpikeyLoader::getViewDIR().'plusGrandNombre.view.php';
}