<?php

function pageEnregistrerFormulaire() {
    $donneesFormulaire = $_POST;
    
    try {
        $remplirBDD = new RemplirBDD();
        $_SESSION['idObjetsFormulaire'] = $remplirBDD->UploadFormulaire($donneesFormulaire);
        aff_r($_POST);
        header('Location: index.php?act=generationPdf');
    }
    catch(Exception $erreurEnvoiBDD) {
        return false;
    }
}

function genererMailPourFormulaire($mailDestinataire) {
    $mail['destinataire'] = strtolower(htmlspecialchars((String) $mailDestinataire));

    if(!preg_match('#@(hotmail|live|msn)#', $mail['destinataire']))
        $endl = "\r\n";
    else
        $endl = "\n";

    //$nomUtilisateur preg_replace();

    $mail['objet'] = 'Formulaire étudiant';
    $urlDuSite = 'http://localhost/Projets/Form2Pdf//index.php?act=presenterFormulaire';

    ob_start(); ?>
        <style>
            body { font-family: arial; }
            h1 { font-size: 17px; margin: 0px; color: rgb(243, 133, 30); }
            h2 { font-size: 15px; margin: 0px; color: rgb(243, 133, 30); }
        </style>
        <body>
            Mr/Mme,<br />
            Le CFA Utec souhaite évaluer votre apprenti le temps d'un formulaire.<br />
            Veuillez fournir vos informations à <a href="<?= $urlDuSite ?>">cette adresse</a>.<br />
            <br />
            <h1><?= $_SESSION['user']['prenom'] ?> <?= $_SESSION['user']['nom'] ?></h1>
            <h2>Formateur au CFA UTEC</h2>
            <img src="https://forum-alternance.fr/wordpress/wp-content/uploads/2016/07/UTEC-logo-1.jpg" style="width:130px; height: 30px"/>
        </body>
    <?php $mail['message'] = ob_get_clean();
    
    $mail['headers'] = 'From: <come.wasik@orange.fr> "CFA UTEC"' . $endl;
    $mail['headers'] .= 'MIME-Version: 1.0' . $endl;
    $mail['headers'] .= "Content-type:text/html;charset=UTF-8" . $endl;
    return $mail;
}

function pageEnvoyerMailFormulaire() {
    $mailDestinataire = $_POST['mailDestinataire'];
    $mail = genererMailPourFormulaire($mailDestinataire);
    
    if(mail($mail['destinataire'], $mail['objet'], $mail['message'], $mail['headers'])) {
        $_SESSION['mailIsSend'] = true;
        header('Location: index.php?act=demandeEnvoyerMail');
    }
    else
        throw new Exception('Le mail n\'a pas pu être envoyé', 3);
}

function pageDeconnexion() {
    //foreach($_SESSION['user'] as $element) { $element = null; }
    session_destroy();
    header('Location: index.php?act=demandeConnexion');
}