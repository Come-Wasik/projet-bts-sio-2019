<?php
/*--------------------------------------
  | Developped by Côme Wasik / nolikein |
  -------------------------------------*/
  /*-----------------
  | About this file |
  -------------------
        Description :
        -------------
            This file allow to init the website by architecture MVC :
                + My Framework
                + Model
                    + External librairies / packages
                    + Internal librairies / packages
                + Controller
                + Init functions (like a function which can starting the session)
                + Others functions (like a debug function)
                + Init actions (like init the session)
*/

/* Framework */
require_once(__DIR__.'/model/SpikeyLoader/SpikeyLoader.class.php');

/* External librairies */
require_once(__DIR__.'/model/vendor/autoload.php');

/* Internal librairies */
require_once(__DIR__.'/model/CideRUseDemise/CideRUseDemise.ini.php');
foreach(['Apprenti', 'Classe_has_Apprenti', 'Classe', 'Entreprise', 'Formateur', 'Formulaire', 'Tuteur'] as $file) {
    require_once(__DIR__.'/model/objects4Database/' . $file . '.class.php'); }
foreach(['RemplirBDD', 'ManualManager'] as $file) {
    require_once(__DIR__.'/model/class/' . $file . '.class.php'); }

/* Controller */
require_once(__DIR__.'/controller/frontend.php');
require_once(__DIR__.'/controller/backend.php');


/* Init functions */
function start_session_module() {
    if(session_status() == PHP_SESSION_NONE)
        if(session_start() === false)
            die('Rechargez la page');
}

/* Others functions */
function aff_r($content) {
    echo '<pre>';
    print_r($content);
    echo '</pre>';
}

function reloadToDefaultPage() {
    if(!empty($_SESSION['idObjetsFormulaire']))
        $_SESSION['idObjetsFormulaire'] = null;

    header('Location: index.php');
}

function get2LineOfFile($nomFichier) {
    if(!$handle = fopen('public/dates/'. $nomFichier, 'r')) {
        return null;
    }
    $line[] = fgets($handle);
    $line[] = fgets($handle);
    fclose($handle);
    return $line;
}

function readLinesOfFile($nomFichier) {
    if(!$handle = fopen('public/dates/'. $nomFichier, 'r'))
        return;
    while(false !== $line = fgets($handle))
        yield $line;

    fclose($handle);
    yield;
}

/* Primary actions in this project */
start_session_module();



