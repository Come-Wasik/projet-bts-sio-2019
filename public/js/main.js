// Activation / désactivation des commentaires de points
    // Q1
function enableEstPonctuelCom() {
    document.getElementById("commentaire_estPonctuel").disabled = "";
    document.getElementById("commentaire_estPonctuel").disabled = "";
}

function disableEstPonctuelCom() {
    document.getElementById("commentaire_estPonctuel").disabled = "disabled";
    document.getElementById("commentaire_estPonctuel").disabled = "disabled";
}

    // Q2
function enableCompConfCom() {
    document.getElementById("commentaire_compConf").disabled = "";
    document.getElementById("commentaire_compConf").disabled = "";
}

function disableCompConfCom() {
    document.getElementById("commentaire_compConf").disabled = "disabled";
    document.getElementById("commentaire_compConf").disabled = "disabled";
}

    // Q3
function enableEstMotiveCom() {
    document.getElementById("commentaire_estMotive").disabled = "";
    document.getElementById("commentaire_estMotive").disabled = "";
}

function disableEstMotiveCom() {
    document.getElementById("commentaire_estMotive").disabled = "disabled";
    document.getElementById("commentaire_estMotive").disabled = "disabled";
}

    // Q5
function enableImportantPointCom() {
    document.getElementById("pointImportant1").disabled = "";
    document.getElementById("pointImportant2").disabled = "";
}

function disableImportantPointCom() {
    document.getElementById("pointImportant1").disabled = "disabled";
    document.getElementById("pointImportant2").disabled = "disabled";
}

// Remplissage progressif de champs
function changeNames() {
    var elementsClasse = document.getElementsByClassName("afficherNomEtPrenom");
    var champTxt = document.getElementById("nomFormateur");
    console.log(champTxt.value);
    for (var i in elementsClasse) {
        elementsClasse[i].innerHTML = champTxt.value;
    }
}

function changerChampTelephonePourConclure() {
    var champAModifier = document.getElementById("telephoneAVerifier");
    var champTxt = document.getElementById("tel_tuteur");

    champAModifier.value = champTxt.value;
}

function onstart() {
    document.getElementById("pointImportant1").disabled = "disabled";
    document.getElementById("pointImportant2").disabled = "disabled";
}