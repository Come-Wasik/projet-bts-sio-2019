<?php $titreDuDocument = "Tests" ?>

<?php $header = 'Page debug du fomulaire' ?>

<?php ob_start(); ?>
<p>
        <strong>DONNEES ISSUE DIRECTEMENT DU FORMULAIRE : </strong><br />
        Nom de l'apprenti : <?= $donneesFormulaire['nomApprenti']?><br />
        Prenom de l'apprenti : <?= $donneesFormulaire['prenomApprenti']?> <br />
        Classe de l'apprenti : <?= $donneesFormulaire['classeApprenti']?> <br />
        Nom de l'entreprise : <?= $donneesFormulaire['nom_entreprise']?> <br />
        Nom du tuteur : <?= $donneesFormulaire['nom_tuteur']?><br />
        Téléphone du tuteur : <?= $donneesFormulaire['tel_tuteur']?> <br />

        L'apprenti est-il ponctuel? <?= $_POST['estPonctuel']?> <br />
        <?php echo 'Commentaire : ', ($donneesFormulaire['commentaire_estPonctuel'] ?? null), '<br />' ?>
        L'apprenti a-t'il un comortement conforme? <?= $donneesFormulaire['comportementConforme']?><br />
        <?php echo 'Commentaire : ', ($donneesFormulaire['commentaire_compConf'] ?? null), '<br />' ?>
        L'apprenti est-il motivé? <?= $donneesFormulaire['estMotive']?> <br />
        <?php echo 'Commentaire : ', ($donneesFormulaire['commentaire_estMotive'] ?? null), '<br />' ?>
        Y-a-t'il des points que vous pensez important à noter? <?= $donneesFormulaire['noterPointsImportants']?> <br />
        <?php echo 'Commentaire : ', ($donneesFormulaire['pointImportant1'] ?? null), '<br />' ?>
        <?php echo 'Commentaire : ', ($donneesFormulaire['pointImportant2'] ?? null), '<br />' ?>
        Mail du tuteur : <?= $donneesFormulaire['mailAVerifier']?> <br />
        Acceptez vous l'offre de promotion? <?= $donneesFormulaire['acceptePromotion']?> <br />

        <br /><strong>DONNEES OBJET CLASSE : </strong><br />
        Nom de la classe : <?= $classe->getNomClasse() ?> <br />

        <br /><strong>DONNEES OBJET ENTREPRISE : </strong><br />
        Nom de l'entreprise : <?= $entreprise->getNomEntreprise() ?> <br />

        <br /><strong>DONNEEES OBJET TUTEUR : </strong><br />
        Nom du tuteur : <?= $tuteur->getNomTuteur()?> <br />
        Téléphone du tuteur :  <?= $tuteur->getTelTuteur()?> <br />
        Mail du tuteur : <?= $tuteur->getMailTuteur()?><br />
        Id de l'entreprise : <?= $tuteur->getIdEntreprise()?> <br />
        + Nom de l'entreprise : <?= $CideRUseDemise->get($tuteur->getIdEntreprise(), 'Entreprise')->getNomEntreprise()?> <br />

        <br /><strong>DONNEES OBJET APPRENTI : </strong><br />
        Prénom de l'apprenti : <?= $apprenti->getPrenomApprenti() ?> <br />
        Nom de l'apprenti : <?= $apprenti->getNomApprenti() ?> <br />
        Id de l'entreprise de l'apprenti : <?= $apprenti->getIdEntreprise()?> <br />
        + Nom de l'entreprise : <?= $CideRUseDemise->get($apprenti->getIdEntreprise(), 'Entreprise')->getNomEntreprise()?> <br />
        Id du tuteur de l'apprenti : <?= $apprenti->getIdTuteur() ?> <br />
        + Nom du tuteur de l'apprenti : <?= $CideRUseDemise->get($apprenti->getIdTuteur(), 'Tuteur')->getNomTuteur() ?> <br />
        Nombre de retards : <?= $apprenti->getNbRetards() ?> <br />
        Nombre d'abcences : <?= $apprenti->getNbAbsences() ?> <br />

        <br /><strong>DONNEES OBJET CLASSE_HAS_APPRENTI : </strong><br />
        Id de la classe : <?= $lien_classe_et_apprenti->getIdClasse() ?> <br />
        + Nom de la classe : <?= $CideRUseDemise->get($lien_classe_et_apprenti->GetIdClasse(), 'Classe')->getNomClasse() ?> <br />
        Id de l'apprenti : <?= $lien_classe_et_apprenti->getIdApprenti() ?> <br />
        + Prénom et Nom de l'apprenti : <?php $apprenti = $CideRUseDemise->get($lien_classe_et_apprenti->getIdApprenti(), 'Apprenti');
                echo $apprenti->getPrenomApprenti(), ' ', $apprenti->getNomApprenti() ?> <br /> 

        <br /><strong>DONNEES OBJET FORM : </strong><br />
        Année actuelle : <?= $formulaire->getAnneeActuelle()?> <br />
        Est ponctuel? : <?= $formulaire->getEstPonctuel()?> <br />
        Com ponctuel : <?= $formulaire->getCommentaire_estPonctuel()?> <br />
        Comportement conforme? : <?= $formulaire->getComportementConforme()?> <br />
        Com Comp Conf : <?= $formulaire->getCommentaire_compConf()?> <br />
        Est motivé? : <?= $formulaire->getEstMotive()?> <br />
        Com est motivé : <?= $formulaire->getCommentaire_estMotive()?> <br />
        Noter point(s) important? : <?= $formulaire->getNoterPointsImportants()?><br />
        Point 1 : <?= $formulaire->getPointImportant1()?> <br />
        Point 2 : <?= $formulaire->getPointImportant2()?> <br />
        Accepter promotion : <?= $formulaire->getAcceptePromotion()?> <br />
</p>

<script>
        document.body.style.background = "#f3f3f3 url('public/images/WallpaperForTests.png') no-repeat right top"; 
</script>
<?php $content = ob_get_clean(); ?>

        <?php include 'view/_template.php'; ?>