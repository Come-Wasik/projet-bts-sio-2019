<?php $titreDuDocument = 'TP3' ?>

<?php $header = 'Debug Mode' ?>

<?php ob_start(); ?>
<main id="document">
    <h1>Bienvenue à l'accueil!</h1>
    <p>
        Sélectionnez votre page :
        <ul>
            <li><a>Frontend</a></li>
            <ul>
                <li><a href="index.php?act=accueil">Accueil</a></li>
                <li><a href="index.php?act=presenterFormulaire">Aller sur le formulaire à remplir</a></li>
                <li><a href="index.php?act=generationPdf">Génère le pdf à partir des données du précédant formulaire (requis : save form to db)</a></li>
                <li><a href="index.php?act=demandeConnexion">Se loguer (requis : déconnexion)</a></li>
                <li><a href="index.php?act=demandeEnvoyerMail">Envoyer le formulaire à qq (requis : connexion)</a></li>
            </ul>
            <li><a>Backend</a></li>
            <ul>
                <li><a href="index.php?act=enregistrerFormulaire">Enregistrer le formulaire dans la bdd (requis : formulaire)</a></li>
                <li><a href="index.php?act=envoyerMailFormulaire">Envoyer le formulaire (requis : connexion, email)</a></li>
                <li><a href="index.php?act=deconnexion">Se déconnecter (requis : connexion)</a></li>
            </ul>
        </ul>
    </p>
</div>
<?php $content = ob_get_clean(); ?>

<?php require 'view/_template.view.php'; ?>