<?php
function testObjFormulaire() {
    $form = new formulaire(['idFormateur' => 2,
        'estPonctuel' => 1,
        'commentaire_EstPonctuel' => 'Oui il est ponctuel',
        'comportementConforme' => 1,
        'commentaire_compConf' => 'Son comportement était très conforme',
        'estMotive' => 1,
        'commentaire_estMotive' => 'Il ne parle que de son travail',
        'noterPointsImportants' => 1,
        'pointImportant1' => 'Yeeeeeeeeeeeeep',
        'pointImportant2' => 'Nooooooooooooop',
        'acceptePromotion' => 0,
        'anneeActuelle' => 2018]);
    
    echo $form->getIdFormateur(), '<br />';
    echo $form->getEstPonctuel(), '<br />';
    echo $form->getCommentaire_EstPonctuel(), '<br />';
    echo $form->getComportementConforme(), '<br />';
    echo $form->getCommentaire_compConf(), '<br />';
    echo $form->getEstMotive(), '<br />';
    echo $form->getCommentaire_estMotive(), '<br />';
    echo $form->getNoterPointsImportants(), '<br />';
    echo $form->getPointImportant1(), '<br />';
    echo $form->getPointImportant2(), '<br />';
    echo $form->getAcceptePromotion(), '<br />';
    echo $form->getAnneeActuelle(), '<br />';

    echo '<br />';
    
    $form->setIdFormateur(3);
    $form->setEstPonctuel(0);
    $form->setCommentaire_EstPonctuel('Oh oh! Oui!');
    $form->setComportementConforme(1);
    $form->setCommentaire_compConf('Il est même exemplaire!');
    $form->setEstMotive(0);
    $form->setCommentaire_estMotive('NULL');
    $form->setNoterPointsImportants(1);
    $form->setPointImportant1('Euh...');
    $form->setPointImportant2('Oui...?');
    $form->setAcceptePromotion(0);
    $form->setAnneeActuelle(2017);

    echo $form->getIdFormateur(), '<br />';
    echo $form->getEstPonctuel(), '<br />';
    echo $form->getCommentaire_EstPonctuel(), '<br />';
    echo $form->getComportementConforme(), '<br />';
    echo $form->getCommentaire_compConf(), '<br />';
    echo $form->getEstMotive(), '<br />';
    echo $form->getCommentaire_estMotive(), '<br />';
    echo $form->getNoterPointsImportants(), '<br />';
    echo $form->getPointImportant1(), '<br />';
    echo $form->getPointImportant2(), '<br />';
    echo $form->getAcceptePromotion(), '<br />';
    echo $form->getAnneeActuelle(), '<br />';
}

function testManagerFormulaire() {
    $formulaire = new formulaire(['idFormateur' => 1,
        'estPonctuel' => 1,
        'commentaire_EstPonctuel' => 'Oui il est ponctuel',
        'comportementConforme' => 1,
        'commentaire_compConf' => 'Son comportement était très conforme',
        'estMotive' => 1,
        'commentaire_estMotive' => 'Il ne parle que de son travail',
        'noterPointsImportants' => 1,
        'pointImportant1' => 'Yeeeeeeeeeeeeep',
        'pointImportant2' => 'Nooooooooooooop',
        'acceptePromotion' => 0,
        'anneeActuelle' => 2018]);

    $manager = new FormulaireManager(DbMainStarter::useMySqlDb());
    //$manager->add($formulaire);
    //INSERT INTO Formulaires (estPonctuel, commentaire_estPonctuel, comportementConforme, commentaire_compConf, estMotive, commentaire_estMotive, noterPointsImportants, pointImportant1, pointImportant2, mailAVerifier, telephoneAVerifier, acceptePromotion, anneeActuelle) VALUES (1, 'a', 1, 'a', 1, 'a', 0, NULL, NULL, 'emo@enu', '0646464646', 1, '22-06-2016');
    
    $form = $manager->get(1);

    echo $form->getIdFormateur(), '<br />';
    echo $form->getEstPonctuel(), '<br />';
    echo $form->getCommentaire_EstPonctuel(), '<br />';
    echo $form->getComportementConforme(), '<br />';
    echo $form->getCommentaire_compConf(), '<br />';
    echo $form->getEstMotive(), '<br />';
    echo $form->getCommentaire_estMotive(), '<br />';
    echo $form->getNoterPointsImportants(), '<br />';
    echo $form->getPointImportant1(), '<br />';
    echo $form->getPointImportant2(), '<br />';
    echo $form->getAcceptePromotion(), '<br />';
    echo $form->getAnneeActuelle(), '<br />';
    
    echo 'Fin du code';
}

function testObjEntreprise() {
    $entreprise = new Entreprise(['idEntreprise' => 1, 'nomEntreprise' => 'Yooka']);

    echo $entreprise->getIdEntreprise(), '<br />';
    echo $entreprise->getNomEntreprise(), '<br />';

    $entreprise->setIdEntreprise(2);
    $entreprise->setNomEntreprise('Laylee');

    echo $entreprise->getIdEntreprise(), '<br />';
    echo $entreprise->getNomEntreprise(), '<br />';
}

function testManagerEntreprise() {
    $mouleEntreprise = new Entreprise(['nomEntreprise' => 'Test']);
    $manager = new EntrepriseManager(MySqlDb::getMyDb());

    $manager->add($mouleEntreprise);
    $entreprises = $manager->getList();

    $entreprise = $manager->get(1);

    echo 'TEST GET <br />';
    echo $entreprise->getIdEntreprise(), '<br />';
    echo $entreprise->getNomEntreprise(), '<br />';

    echo 'TEST GETLIST <br />';
    foreach($entreprises as $entreprise) {
        echo $entreprise->getIdEntreprise(), '<br />';
        echo $entreprise->getNomEntreprise(), '<br />';
        echo '<br />';
    }

}

function testObjClasse() {
    $classe = new Classe(['idClasse' => 1, 'nomClasse' => '2BTSSM']);

    echo $classe->getIdClasse(), '<br />';
    echo $classe->getNomClasse(), '<br />';

    $classe->setIdClasse(2);
    $classe->setNomClasse('1BTSSIOSLAM');

    echo $classe->getIdClasse(), '<br />';
    echo $classe->getNomClasse(), '<br />';
}

function testManagerClasse() {
    $mouleClasse = new Classe(['nomClasse' => '2BTSSIOSLAM']);
    $manager = new ClasseManager(MySqlDb::getMyDb());

    $manager->add($mouleClasse);

    $classe = $manager->get(1);
    $classes = $manager->getList();

    echo 'TEST GET <br />';
    echo $classe->getIdClasse(), '<br />';
    echo $classe->getNomClasse(), '<br />';

    echo 'TEST GETLIST <br />';
    foreach($classes as $classe) {
        echo $classe->getIdClasse(), '<br />';
        echo $classe->getNomClasse(), '<br />';
        echo '<br />';
    }

}

function testObjTuteur() {
    $tuteur = new Tuteur(['idTuteur' => 1,
        'nomTuteur' => 'JeanMi',
        'telTuteur' => '0646464646',
        'mailTuteur' => 'jean@test.com',
        'idEntreprise' => 5]);
    
    echo $tuteur->getIdTuteur(), '<br />';
    echo $tuteur->getNomTuteur(), '<br />';
    echo $tuteur->getTelTuteur(), '<br />';
    echo $tuteur->getMailTuteur(), '<br />';
    echo $tuteur->getIdEntreprise(), '<br />';

    $tuteur->setIdTuteur(3);
    $tuteur->setNomTuteur('Eca');
    $tuteur->setTelTuteur('0754545454');
    $tuteur->setMailTuteur('aka@mart.com');
    $tuteur->setIdEntreprise(10);

    echo $tuteur->getIdTuteur(), '<br />';
    echo $tuteur->getNomTuteur(), '<br />';
    echo $tuteur->getTelTuteur(), '<br />';
    echo $tuteur->getMailTuteur(), '<br />';
    echo $tuteur->getIdEntreprise(), '<br />';
}

function testManagerTuteur() {
    $tuteurMoule = new Tuteur(['nomTuteur' => 'kizi',
        'telTuteur' => '0646464646',
        'mailTuteur' => 'jean@test.com',
        'idEntreprise' => 5]);
    $manager = new TuteurManager(MySqlDb::getMyDb());
    
    //$manager->add($tuteurMoule);
    $tuteur = $manager->get(1);



    echo $tuteur->getIdTuteur(), '<br />';
    echo $tuteur->getNomTuteur(), '<br />';
    echo $tuteur->getTelTuteur(), '<br />';
    echo $tuteur->getMailTuteur(), '<br />';
    echo $tuteur->getIdEntreprise(), '<br />';

}

function testObjFormateur() {
    $tuteur = new Formateur(['idFormateur' => 1,
        'prenomFormateur' => 'Jean',
        'nomFormateur' => 'Mi',
        'idEntreprise' => 2,
        'idTuteur' => 3]);

    echo $tuteur->getIdFormateur(), '<br />';
    echo $tuteur->getPrenomFormateur(), '<br />';
    echo $tuteur->getNomFormateur(), '<br />';
    echo $tuteur->getIdEntreprise(), '<br />';
    echo $tuteur->getIdTuteur(), '<br />';

    $tuteur->setIdFormateur(4);
    $tuteur->setPrenomFormateur('Eric');
    $tuteur->setNomFormateur('Dambert');
    $tuteur->setIdEntreprise(5);
    $tuteur->setIdTuteur(6);

    echo $tuteur->getIdFormateur(), '<br />';
    echo $tuteur->getPrenomFormateur(), '<br />';
    echo $tuteur->getNomFormateur(), '<br />';
    echo $tuteur->getIdEntreprise(), '<br />';
    echo $tuteur->getIdTuteur(), '<br />';
}


function testManagerFormateur() {
    $tuteurMoule = new Formateur(['prenomFormateur' => 'Elo',
        'nomFormateur' => 'Marc',
        'idEntreprise' => 2,
        'idTuteur' => 3]);

    $manager = new FormateurManager(MySqlDb::getMyDb());

    //$manager->add($tuteurMoule);
    $tuteur = $manager->get(2);
    //$manager->delete($tuteur);
    //$tuteur->setPrenomFormateur('Eric');
    //$tuteur->setNomFormateur('Meo');

    echo $tuteur->getIdFormateur(), '<br />';
    echo $tuteur->getPrenomFormateur(), '<br />';
    echo $tuteur->getNomFormateur(), '<br />';
    echo $tuteur->getIdEntreprise(), '<br />';
    echo $tuteur->getIdTuteur(), '<br />';

    $manager->update($tuteur);
}

function testObjCHA() {
    $C_H_A = new Classe_has_Formateurs(['idTable' => 1,
        'idClasse' => 2,
        'idFormateur' => 3,
        'anneeActuelle' => 2018]);

    echo $C_H_A->getIdTable(), '<br />';
    echo $C_H_A->getIdClasse(), '<br />';
    echo $C_H_A->getIdFormateur(), '<br />';
    echo $C_H_A->getAnneeActuelle(), '<br />';

    $C_H_A->setIdTable(4);
    $C_H_A->setIdClasse(5);
    $C_H_A->setIdFormateur(6);
    $C_H_A->setAnneeActuelle(2019);

    echo $C_H_A->getIdTable(), '<br />';
    echo $C_H_A->getIdClasse(), '<br />';
    echo $C_H_A->getIdFormateur(), '<br />';
    echo $C_H_A->getAnneeActuelle(), '<br />';
}

function testManagerCHA() {
    $C_H_AMoule = new Classe_has_Formateurs(['idClasse' => 2,
        'idFormateur' => 3,
        'anneeActuelle' => 2018]);

    $manager = new Classe_has_FormateursManager(MySqlDb::getMyDb());

    $manager->add($C_H_AMoule);

    $C_H_A = $manager->get(1);


    echo $C_H_A->getIdTable(), '<br />';
    echo $C_H_A->getIdClasse(), '<br />';
    echo $C_H_A->getIdFormateur(), '<br />';
    echo $C_H_A->getAnneeActuelle(), '<br />';

}